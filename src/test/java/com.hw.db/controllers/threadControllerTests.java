package com.db.controllers;

import com.hw.db.controllers.threadController;
import com.hw.db.DAO.UserDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import com.hw.db.models.User;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


class threadControllerTests {
    private Thread thread;

    @BeforeEach
    @DisplayName("Thread controller tests")
    void beforeThreadTests() {
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());

        thread = new Thread("Timur", ts, "forum", "message", "slug", "title", 0);
    }

    @Test
    @DisplayName("Testing change method with not-found")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(null);
            threadMock.when(() -> ThreadDAO.change(null, thread)).thenThrow(new DataAccessException("Раздел не найден."){});
            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new com.hw.db.models.Message("Раздел не найден.")).getStatusCode(), controller.change("slug", thread).getStatusCode(), "Thread is no found");
        }
    }

    @Test
    @DisplayName("Testing info method")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info("slug"), "Info was successfully retrieved");
        }
    }

    @Test
    @DisplayName("Testing createVote method")
    void testCreateVote() {
        User user = new User("anufriyevt", "email", "Timur", "about");
        Vote vote = new Vote("anufriyevt", 5);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {

                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("anufriyevt")).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), controller.createVote("slug", vote).getStatusCode(), "Vote was successfully created");
            }
        }
    }

    @Test
    @DisplayName("Testing CheckIdOrSlug method")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt("0"))).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug("slug"), "Found thread by slug");
            assertEquals(thread, controller.CheckIdOrSlug("0"), "Found thread by id");
            assertNull(controller.CheckIdOrSlug("1"), "Not found slug");
            assertNull(controller.CheckIdOrSlug("slug123"), "Not found id");
        }
    }

    @Test
    @DisplayName("Testing createPost method succesfully")
    void testCreatePost() {
        List<Post> posts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), controller.createPost("slug", posts), "Post was created");
            }
        }
    }

    @Test
    @DisplayName("Testing Posts method succesfully")
    void testPosts() {
        List<Post> posts = Collections.emptyList();;
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), controller.Posts("slug", 5, 1, "tree", true), "Posts were retrieved");
        }

    }

}
